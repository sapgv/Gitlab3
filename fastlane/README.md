fastlane documentation
----

# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```sh
xcode-select --install
```

For _fastlane_ installation instructions, see [Installing _fastlane_](https://docs.fastlane.tools/#installing-fastlane)

# Available Actions

## iOS

### ios updateCertificateProvision

```sh
[bundle exec] fastlane ios updateCertificateProvision
```

UPDATE CERTIFICATE RELEASE PROVISION

### ios updateProjectCertificateProvision

```sh
[bundle exec] fastlane ios updateProjectCertificateProvision
```

UPDATE PROJECT CERTIFICATE RELEASE PROVISION

### ios archive

```sh
[bundle exec] fastlane ios archive
```

ARCHIVE APP

### ios export

```sh
[bundle exec] fastlane ios export
```

EXPORT IPA WITH FRESH RELEASE PROVISION

### ios git

```sh
[bundle exec] fastlane ios git
```

GIT PROMT

### ios gitPush

```sh
[bundle exec] fastlane ios gitPush
```

GIT COMMIT, TAG, UPDATE

### ios sentryUploadDsym

```sh
[bundle exec] fastlane ios sentryUploadDsym
```

UPLOAD DSYM TO SENTRY

### ios release

```sh
[bundle exec] fastlane ios release
```

CREATE NEW SENTRY RELEASE

### ios setReleaseCommit

```sh
[bundle exec] fastlane ios setReleaseCommit
```

ASSOCIATING COMMITS TO RELEASE

### ios increment

```sh
[bundle exec] fastlane ios increment
```

INCREMENT BUILD VERSION

----

This README.md is auto-generated and will be re-generated every time [_fastlane_](https://fastlane.tools) is run.

More information about _fastlane_ can be found on [fastlane.tools](https://fastlane.tools).

The documentation of _fastlane_ can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
