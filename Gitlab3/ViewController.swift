//
//  ViewController.swift
//  Gitlab3
//
//  Created by Grigory Sapogov on 30.06.2022.
//

import UIKit
import Sentry

class ViewController: UIViewController {

    @IBOutlet weak var label: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        #if DEBUG
        self.label.text = "DEBUG"
        #else
        self.label.text = "RELEASE"
        #endif
        
        
    }

    @IBAction func actionItem(_ sender: Any) {
        SentrySDK.crash()
    }
    
    @IBAction func item2(_ sender: Any) {
        
    }
}

